import IO from 'koa-socket';

const io = new IO();

export default io;
