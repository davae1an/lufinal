import Koa from 'koa';
import koaBody from 'koa-body';
import cors from '@koa/cors';
import io from './socketio2';

const app = new Koa();

app.use(koaBody());
app.use(cors());
// app.use(flowRouter.routes());
// app.use(flowRouter.allowedMethods());

io.attach(app);

io.on('join', (ctx, data) => {
  console.log('Client Joined');
  console.log(data);
});

// io.on('status', (ctx, data) => {
//   ctx.socket.broadcast('status', data);
// });

io.on('disconnect', () => {
  console.log('Client disconnected');
});

io.on('connect', () => {
  console.log('Client connected');
});

io.on('expstart', (ctx, data) => {
  console.log('expstarting command received');
  console.log(data);
  ctx.socket.broadcast('expstart', data);
});

io.on('expstop', (ctx, data) => {
  console.log('exp stoping command received');
  console.log(data);
  ctx.socket.broadcast('expstop', data);
});

io.on('check', (ctx, data) => {
  console.log('check received');
  ctx.socket.broadcast('check', data);
});

io.on('exp_running', (ctx, data) => {
  ctx.socket.broadcast('exp_check', data);
});

app.use(async (ctx) => {
  ctx.body = 'Pi Hub Server';
});

app.listen(8000);
