import { Box, Button, Heading } from 'grommet';
import React from 'react';
import PropTypes from 'prop-types';

const Slide1 = ({store, socket, ...rest}) => (
  <Box align="center" pad="large" justify="between" full>
    <Heading size="xlarge">{store.state.exp1running.toString()}</Heading>
    <Button label="Start" onClick={() => {socket.expstart('1')}} {...rest} />
    <Button margin={{ top: "small" }} label="Stop" onClick={() => {socket.expstop('1')}} {...rest} />
  </Box>
);

Slide1.propTypes = {
  store: PropTypes.object.isRequired,
  socket: PropTypes.object.isRequired,
};

export default Slide1;
