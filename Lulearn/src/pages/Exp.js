import React from 'react';
import { Box, Heading } from 'grommet';

const Exp = (props) => (
    <Box align="center" pad="large" justify="between" full>
      <Heading size="xlarge">{props.store.state.exp1running.toString()}</Heading>
    </Box>
  )

export default Exp;
