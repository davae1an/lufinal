import {Grommet} from 'grommet';
import {grommet} from 'grommet/themes';
import React from 'react';
import MainRoute from './routes/main-route';


const App = () => (
      <Grommet theme = {grommet} full>
        <MainRoute />
      </Grommet>
)


export default App;
