import io from 'socket.io-client';
import configs from './configs/configs';

/* NB. it should name SocketContainerInstance but due to using export default
I can name it anything like SockStateIn */
import SockStateIn from './state/state'

class Socketz {

  constructor() {
    this.socket = io(configs.SERVER_URL);


    this.socket.on('connect', () => {
      console.log('Connected to server');

      this.socket.emit('join', 'Web App Connected');
      this.socket.emit('check', 'check')
      
      // Change state showing that isConnected true 
      SockStateIn.changeConnect(true)

     
      this.socket.on('disconnect', () => {
        // it disconnected show status as false
        SockStateIn.changeConnect(false)
      });

      this.socket.on('expstart', (data) => {
         console.log(`exp ${data} started`)

         if (data === "1") {
           console.log("experiment one status recieved")
           SockStateIn.changeExp1(true)

         }
      });

      this.socket.on('check', (data) => {
        console.log(data)

        if (data.exp === "1") {
          console.log("status check received from exp1")
          SockStateIn.changeExp1(data.status)

        }
     });

      this.socket.on('expstop', (data) => {
        console.log(`exp ${data} stopped`)

        if (data === "1") {
          console.log("experiment one status recieved stopped")
          SockStateIn.changeExp1(false)
        }
     });

      this.socket.on('reconnect', () => {
        SockStateIn.changeConnect(true)
      });

      this.socket.on('join', (data) => {
        console.log(`Recieved: ${data}`);
      });
    });
  }

  // checks the current state of all experiments
  checkall() {
    this.socket.emit('check', 'checking');
  }

  // Start specific experiment
  expstart(exp) {
      this.socket.emit('expstart', exp);
  }

  // Stop a specific experimen
  expstop(exp) {
    this.socket.emit('expstop', exp);
  }

  // Stops all experiments
  stopall() {
    this.socket.emit('stopall', 'stop');
  }


}

const SocketClient = new Socketz();

export default SocketClient;
