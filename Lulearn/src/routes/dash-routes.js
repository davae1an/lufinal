import { Box, Button, Grid, Text } from 'grommet';
import { Home, Test, Square, StatusCritical } from 'grommet-icons';
import PropTypes from 'prop-types';
import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { Subscribe } from 'unstated';
import Exp from '../pages/Exp';
import Slide1 from '../pages/Slide1';
import SocketContainerInstance from '../state/state';
import SocketClient from '../Socketz';

// For the side bar
const routesWithIcons = (match, iconProps) => [
  {
    name: 'Home',
    path: `${match.path}/slide1`,
    icon: <Home {...iconProps} />
  },

  {
    name: 'Experiments',
    path: `${match.path}/exp`,
    icon: <Test {...iconProps} />
  }
];



const DashMainRoutes = props => {
  const { match, history } = props;

 
  return (
    <Subscribe to={[SocketContainerInstance]}>
    {(socketstat) => (
    <Grid
      fill
      areas={[
        { name: 'nav', start: [0, 0], end: [0, 0] },
        { name: 'main', start: [1, 0], end: [1, 0] }
      ]}
      columns={['115px', 'flex']}
      rows={['flex']}
    >
      <Box gridArea="nav" background="neutral-3">

      <Box
      align="center"
      margin={{ top: 'xsmall' }}
      pad={{ horizontal: 'medium', vertical: 'small' }}
      >
          {socketstat.state.isConnected ? <Square color="status-ok" size="large" /> : <StatusCritical color="status-critical" size="large" />}
          <Text margin={{ top: 'xsmall' }}>Status</Text>
      </Box>
        <Box margin={{ top: 'small' }}>
          {routesWithIcons(match, { size: 'large' }).map(obj => (
            <Button
              key={obj.name}
              hoverIndicator
              onClick={() => history.push(obj.path)}
            >
              <Box
                align="center"
                pad={{ horizontal: 'medium', vertical: 'small' }}
              >
                {obj.icon}
                <Text margin={{ top: 'xsmall' }}>{obj.name}</Text>
              </Box>
            </Button>
          ))}

       
        </Box>

      </Box>
      <Box gridArea="main">
        <Switch>
          <Route path={`${match.path}/slide1`} render={() => <Slide1 {...props} store={socketstat} socket={SocketClient} />}/>
          <Route path={`${match.path}/exp`} render={() => <Exp {...props}  store={socketstat}/>} socket={SocketClient} />
          <Redirect to={`${match.path}/slide1`} />
        </Switch>
      </Box>
    </Grid>
    )}
    </Subscribe>
  );
};

DashMainRoutes.propTypes = {
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired
};

export default DashMainRoutes;
