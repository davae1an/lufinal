import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import DashRoute from './dash-routes';

const MainRoute = () => (
  <Switch>
    <Route path="/u" component={DashRoute} />
    <Redirect to="/u" />
  </Switch>
);

export default MainRoute;

// <Route path='/slide2\' component={Slide2} /'
// <Route path='/slide3' component={
// Slide3} />
// <Redirect to="/slide1' /
