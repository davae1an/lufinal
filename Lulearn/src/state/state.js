
import {  Container } from 'unstated';

class SocketContainer extends Container {
    state = { 
        isConnected: false, 
        exp1running: false
    };

    changeConnect = ( status ) => {
        this.setState({ isConnected: status });
    }

    changeExp1 = ( status ) => {
      this.setState({ exp1running: status })
    }
  }


// created a instance in this file so it can be shared
const SocketContainerInstance = new SocketContainer();

export default SocketContainerInstance


