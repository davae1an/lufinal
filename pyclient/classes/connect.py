# from socketIO_client import BaseNamespace
# from classes.gpiostuff import *
# import datetime
# import subprocess
# import time


class Status(object):
    isConnected = False
    isRunning = False

    # def SortData(data, char1, char2):
    #     Sorted = data[data.find(char1) + 1: data.find(char2)]
    #     return Sorted


# class Namespace(BaseNamespace):

#     def on_connect(self):
#         Status.isConnected = True
#         print('Connected to server')
#         self.emit('join', 'raspberrypi joined at ' +
#                   str(datetime.datetime.now()))

#     def on_reconnect(self):
#         print('raspberry pi reconnected')
#         self.emit('join', 'raspberry pi reconnected')
#         Status.isConnected = True

#     def on_disconnect(self):
#         Status.isConnected = False
#         print('Disconnected from the server trying to reconnect')

#     def on_vavlepi(self, *args):
#         data = Status.SortData(str(args), '{', '}')
#         print(data)
#         if data == 'on':
#             led5.on()
#             led6.off()
#             self.emit('valvechanged', 'on')
#             Status.valve = 'ON'
#             time.sleep(4)
#             # led5.off()
#             # led6.off()
#         else:
#             led5.off()
#             led6.on()
#             Status.valve = 'OFF'
#             self.emit('valvechanged', 'off')
#             time.sleep(4)
#             # led5.off()
#             # led6.off()

#     def on_picheck(self, *args):
#         data = Status.SortData(str(args), '{', '}')
#         if data == 'ping':
#             self.emit('pistatus', 'pong')
#             print('Response pong was sent')

#     def on_reboot(self, *args):
#         lcd.message('Rebooting' + '\n' + 'Zeroing ')
#         time.sleep(4)
#         subprocess.call("/sbin/shutdown -r now", shell=True,
#                         stdout=subprocess.PIPE, stderr=subprocess.PIPE)
