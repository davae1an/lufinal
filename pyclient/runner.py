# from classes.connect import Status
from threading import Thread
import socketio

# initialize state as false
ExperimentRunning = False

# asyncio
sio = socketio.Client()


@sio.on('connect')
def on_connect():
    print('I\'m connected!')
    sio.emit('join', "exp1 connected")


@sio.on('reconnect')
def on_reconnect():
    print('I reconnected!')


@sio.on('check')
def on_check(data):
    sio.emit('check', {'exp': '1', 'status': ExperimentRunning})


@sio.on('expstart')
def on_expstart(data):
    # global is used to access ExperimentRunning outside
    #  the scope or else it would be locally scoped
    global ExperimentRunning
    print('exp starting')
    print(data)

    if data == "1":
        print("im experiment one so im gonna do something")
        # Trigger something here like a led to turn on

    # Send status that its on back to socket server

    ExperimentRunning = True
    sio.emit('expstart', "1")


@sio.on('expstop')
def on_expstop(data):

    global ExperimentRunning
    print('exp stopped')
    print(data)

    if data == "1":
        print("i stopped experimenting")
        # Trigger something here like a led to turn off

    # Send status that its on back to socket server

    ExperimentRunning = False
    sio.emit('expstop', "1")


@sio.on('disconnect')
def on_disconnect():
    print('I\'m disconnected!')


# socketIO = SocketIO('localhost', 3000, Namespace)


def connector():
    sio.connect('http://localhost:8000')


clientconnector = Thread(target=connector)


clientconnector.start()
